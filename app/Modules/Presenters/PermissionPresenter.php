<?php

namespace App\Modules\Presenters;

use App\Modules\Transformers\PermissionTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class PermissionPresenter extends FractalPresenter
{

    public function getTransformer()
    {
        return new PermissionTransformer();
    }
}
