<?php

namespace App\Modules\Presenters;

use App\Modules\Transformers\AlunoTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AlunoPresenter.
 *
 * @package namespace App\Modules\Presenters;
 */
class AlunoPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AlunoTransformer();
    }
}
