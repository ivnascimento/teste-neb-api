<?php

namespace App\Modules\Presenters;

use App\Modules\Transformers\RoleTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class RolePresenter extends FractalPresenter
{

    public function getTransformer()
    {
        return new RoleTransformer();
    }
}
