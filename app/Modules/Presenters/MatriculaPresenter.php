<?php

namespace App\Modules\Presenters;

use App\Modules\Transformers\MatriculaTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class MatriculaPresenter.
 *
 * @package namespace App\Modules\Presenters;
 */
class MatriculaPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MatriculaTransformer();
    }
}
