<?php

namespace App\Modules\Presenters;

use App\Modules\Transformers\CursoTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CursoPresenter.
 *
 * @package namespace App\Modules\Presenters;
 */
class CursoPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CursoTransformer();
    }
}
