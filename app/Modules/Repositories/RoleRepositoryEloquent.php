<?php

namespace App\Modules\Repositories;

use Artesaos\Defender\Role;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Modules\Presenters\RolePresenter;
use App\Modules\Repositories\Contracts\RoleRepository;
use App\Modules\Validators\RoleValidator;

class RoleRepositoryEloquent extends BaseRepository implements RoleRepository
{   
    protected $fieldSearchable = [
        'name' => 'like',
    ];

    public function model()
    {
        return Role::class;
    }

    public function presenter() {
        return new RolePresenter();
    }

    public function validator()
    {
        return RoleValidator::class;
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
