<?php

namespace App\Modules\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Modules\Presenters\AlunoPresenter;
use App\Modules\Repositories\Contracts\AlunoRepository;
use App\Modules\Entities\Aluno;
use App\Modules\Validators\AlunoValidator;

class AlunoRepositoryEloquent extends BaseRepository implements AlunoRepository
{   
    protected $fieldSearchable = [
        'id' => '=',
        'nome' => 'like',
        'cpf' => 'like',
        'data_nascimento' => '=',
        'email' => 'like',
        'telefone' => 'like'
    ];

    public function model()
    {
        return Aluno::class;
    }

    public function presenter() {
        return new AlunoPresenter();
    }

    public function validator()
    {

        return AlunoValidator::class;
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
