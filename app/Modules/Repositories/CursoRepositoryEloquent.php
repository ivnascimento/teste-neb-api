<?php

namespace App\Modules\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Modules\Presenters\CursoPresenter;
use App\Modules\Repositories\Contracts\CursoRepository;
use App\Modules\Entities\Curso;
use App\Modules\Validators\CursoValidator;

class CursoRepositoryEloquent extends BaseRepository implements CursoRepository
{   
    protected $fieldSearchable = [
        'id' => '=',
        'nome' => 'like',
        'cpf' => 'like',
        'data_nascimento' => '=',
        'email' => 'like',
        'telefone' => 'like'
    ];

    public function model()
    {
        return Curso::class;
    }

    public function presenter() {
        return new CursoPresenter();
    }

    public function validator()
    {

        return CursoValidator::class;
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
