<?php

namespace App\Modules\Repositories;

use Artesaos\Defender\Permission;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Modules\Presenters\PermissionPresenter;
use App\Modules\Repositories\Contracts\PermissionRepository;
use App\Modules\Validators\PermissionValidator;

class PermissionRepositoryEloquent extends BaseRepository implements PermissionRepository
{   
    protected $fieldSearchable = [
        'name' => 'like',
    ];

    public function model()
    {
        return Permission::class;
    }

    public function presenter() {
        return new PermissionPresenter();
    }

    public function validator()
    {
        return PermissionValidator::class;
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
