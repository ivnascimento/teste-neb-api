<?php

namespace App\Modules\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Modules\Presenters\UserPresenter;
use App\Modules\Repositories\Contracts\UserRepository;
use App\Modules\Entities\Usuario;
use App\Modules\Validators\UserValidator;

class UserRepositoryEloquent extends BaseRepository implements UserRepository
{   
    protected $fieldSearchable = [
        'id' => '=',
        'nome' => 'like',
        'email' => 'like',
        'situacao' => '=',
        'roles.name' => '='
    ];

    public function model()
    {
        return Usuario::class;
    }

    public function presenter() {
        return new UserPresenter();
    }

    public function validator()
    {

        return UserValidator::class;
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
