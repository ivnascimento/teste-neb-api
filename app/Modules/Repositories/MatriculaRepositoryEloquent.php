<?php

namespace App\Modules\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Modules\Presenters\MatriculaPresenter;
use App\Modules\Repositories\Contracts\MatriculaRepository;
use App\Modules\Entities\Matricula;
use App\Modules\Validators\MatriculaValidator;

class MatriculaRepositoryEloquent extends BaseRepository implements MatriculaRepository
{   
    protected $fieldSearchable = [
        'id' => '=',
        'nome' => 'like',
        'cpf' => 'like',
        'data_nascimento' => '=',
        'email' => 'like',
        'telefone' => 'like',
        'alunos.nome' => 'like',
        'cursos.nome' => 'like',
    ];

    public function model()
    {
        return Matricula::class;
    }

    public function presenter() {
        return new MatriculaPresenter();
    }

    public function validator()
    {

        return MatriculaValidator::class;
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
