<?php

namespace App\Modules\Providers;

use App\Modules\Entities\Usuario;

use App\Modules\Repositories\Contracts\AlunoRepository;
use App\Modules\Repositories\Contracts\CursoRepository;
use App\Modules\Repositories\Contracts\MatriculaRepository;
use App\Modules\Repositories\Contracts\PermissionRepository;
use App\Modules\Repositories\Contracts\RoleRepository;
use App\Modules\Repositories\Contracts\UserRepository;

use App\Modules\Repositories\AlunoRepositoryEloquent;
use App\Modules\Repositories\CursoRepositoryEloquent;
use App\Modules\Repositories\MatriculaRepositoryEloquent;
use App\Modules\Repositories\PermissionRepositoryEloquent;
use Artesaos\Defender\Role;
use App\Modules\Repositories\RoleRepositoryEloquent;
use App\Modules\Repositories\UserRepositoryEloquent;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mapWebRoutes();
    }

    public function register()
    {
        $this->app->bind(AlunoRepository::class, AlunoRepositoryEloquent::class);
        $this->app->bind(CursoRepository::class, CursoRepositoryEloquent::class);
        $this->app->bind(MatriculaRepository::class, MatriculaRepositoryEloquent::class);
        $this->app->bind(PermissionRepository::class, PermissionRepositoryEloquent::class);
        $this->app->bind(RoleRepository::class, RoleRepositoryEloquent::class);
        $this->app->bind(UserRepository::class, UserRepositoryEloquent::class);
    }

    private function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => 'App\Modules\Http\Controllers',
            'prefix' => 'api',
            'as' => 'admin'
        ], function () {
            require app_path('Modules/Http/routes.php');
        });
    }
}
