<?php

use Illuminate\Support\Facades\Route;

foreach(['v1'] as $versao) {
    
    Route::prefix($versao)->group(function () {

        Route::prefix('auth')->group(function () {
            Route::post('login', 'AuthController@login');
        });

        Route::post('usuarios', 'UsuariosController@store');

        Route::middleware('auth:api')->group(function () {

            Route::get('usuarios', 'UsuariosController@index')->middleware('needsPermission:usuario.index');
            Route::get('usuarios/{id}', 'UsuariosController@show')->middleware('needsPermission:usuario.show');
            Route::put('usuarios/{id}', 'UsuariosController@update')->middleware('needsPermission:usuario.update');
            Route::delete('usuarios/{id}', 'UsuariosController@destroy')->middleware('needsPermission:usuario.delete');

            Route::get('roles', 'RolesController@index')->middleware('needsPermission:roles.index');
            Route::get('roles/{id}', 'RolesController@show')->middleware('needsPermission:roles.show');
            Route::post('roles', 'RolesController@store')->middleware('needsPermission:roles.store');
            Route::put('roles/{id}', 'RolesController@update')->middleware('needsPermission:roles.update');
            Route::delete('roles/{id}', 'RolesController@destroy')->middleware('needsPermission:roles.delete');

            Route::get('permissions', 'PermissionsController@index')->middleware('needsPermission:permissions.index');
            Route::get('permissions/{id}', 'PermissionsController@show')->middleware('needsPermission:permissions.show');
            Route::post('permissions', 'PermissionsController@store')->middleware('needsPermission:permissions.store');
            Route::put('permissions/{id}', 'PermissionsController@update')->middleware('needsPermission:permissions.update');
            Route::delete('permissions/{id}', 'PermissionsController@destroy')->middleware('needsPermission:permissions.delete');

            Route::get('alunos', 'AlunosController@index')->middleware('needsPermission:aluno.index');
            Route::get('alunos/{id}', 'AlunosController@show')->middleware('needsPermission:aluno.show');
            Route::post('alunos', 'AlunosController@store')->middleware('needsPermission:aluno.store');
            Route::put('alunos/{id}', 'AlunosController@update')->middleware('needsPermission:aluno.update');
            Route::delete('alunos/{id}', 'AlunosController@destroy')->middleware('needsPermission:aluno.delete');

            Route::get('matriculas', 'MatriculasController@index')->middleware('needsPermission:matricula.index');
            Route::get('matriculas/{id}', 'MatriculasController@show')->middleware('needsPermission:matricula.show');
            Route::post('matriculas', 'MatriculasController@store')->middleware('needsPermission:matricula.store');
            Route::put('matriculas/{id}', 'MatriculasController@update')->middleware('needsPermission:matricula.update');
            Route::delete('matriculas/{id}', 'MatriculasController@destroy')->middleware('needsPermission:matricula.delete');

            Route::get('cursos', 'CursosController@index')->middleware('needsPermission:curso.index');
            Route::get('cursos/{id}', 'CursosController@show')->middleware('needsPermission:curso.show');
            Route::post('cursos', 'CursosController@store')->middleware('needsPermission:curso.store');
            Route::put('cursos/{id}', 'CursosController@update')->middleware('needsPermission:curso.update');
            Route::delete('cursos/{id}', 'CursosController@destroy')->middleware('needsPermission:curso.delete');

            Route::prefix('auth')->group(function () {
                Route::post('logout', 'AuthController@logout');
            });
        });
    });
}