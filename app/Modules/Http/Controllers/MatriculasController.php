<?php

namespace App\Modules\Http\Controllers;

use App\Modules\Repositories\Contracts\MatriculaRepository;
use App\Modules\Services\MatriculaService;
use App\Modules\Transformers\MatriculaTransformer;
use Illuminate\Http\Request;
use App\Core\Http\Controllers\Controller;

class MatriculasController extends Controller
{

    private $matriculaRepository;
    private $matriculaService;

    public function __construct(MatriculaRepository $matriculaRepository, MatriculaService $matriculaService)
    {
        $this->matriculaRepository = $matriculaRepository;
        $this->matriculaService = $matriculaService;
    }

    /**
     * @OA\Get(
     *   path="/matriculas",
     *   tags={"matriculas"},
     *   summary="Lista de matriculas",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="search",
     *     in="query",
     *     description="Filtros (nome).",
     *     required=false,
     *     @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Data limit.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="page",
     *     in="query",
     *     description="Page.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="option",
     *     in="query",
     *     description="Option.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          enum={"1","0"},
     *          default="0"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function index(Request $request)
    {
        $option = $request->get('option', false);
        $request->request->remove('option');

        $retorno = $this->matriculaRepository->paginate($request->get('limit', 15));
        return $this->verificarOptionERetornarDados($option, $retorno);
    }

     /**
     * @OA\Get(
     *   path="/matriculas/{id}",
     *   tags={"matriculas"},
     *   summary="Obter um matricula",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="id do matricula",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          default="1"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="option",
     *     in="query",
     *     description="Option.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          enum={"1","0"},
     *          default="0"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=404, description="not found"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function show(Request $request, $id)
    {
        $option = $request->get('option', false);
        $request->request->remove('option');

        $retorno = $this->matriculaRepository->find($id);
        return $this->verificarOptionERetornarDados($option, $retorno);
    }

    /**
     * @OA\Post(
     *   path="/matriculas",
     *   tags={"matriculas"},
     *   summary="Criar um matricula",
     *   @OA\Parameter(
     *     name="data_matricula",
     *     in="query",
     *     description="Data Matricula (formato: Y-m-d)",
     *     required=true,
     *     @OA\Schema(
     *          type="string",
     *          format="Y-m-d",
     *          default="2000-01-01"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="status",
     *     in="query",
     *     description="Status - A (ativa), B (Bloqueada), C (cancelada), D (desistente), F (Finalizada), T (Trancada)",
     *     required=true,
     *     @OA\Schema(
     *          type="string",
     *          enum={"A","B","C","D","F","T"}
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="aluno_id",
     *     in="query",
     *     description="Id do aluno",
     *     required=true,
     *     @OA\Schema(
     *          type="integer",
     *          default="1"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="curso_id",
     *     in="query",
     *     description="Id do curso",
     *     required=true,
     *     @OA\Schema(
     *          type="integer",
     *          default="1"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function store(Request $request)
    {
        $matricula = $this->matriculaService->store($request->all());
        return $this->retornoPadrao(null, 'Matricula adicionada.', 0, $this->transform($matricula, MatriculaTransformer::class));
    }

    /**
     * @OA\Put(
     *   path="/matriculas/{id}",
     *   tags={"matriculas"},
     *   summary="Atualizar um matricula",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="data_matricula",
     *     in="query",
     *     description="Data Matricula (formato: Y-m-d)",
     *     required=false,
     *     @OA\Schema(
     *          type="string",
     *          format="Y-m-d",
     *          default="2000-01-01"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="status",
     *     in="query",
     *     description="Status - A (ativa), B (Bloqueada), C (cancelada), D (desistente), F (Finalizada), T (Trancada)",
     *     required=false,
     *     @OA\Schema(
     *          type="string",
     *          enum={"A","B","C","D","F","T"}
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="aluno_id",
     *     in="query",
     *     description="Id do aluno",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          default="1"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="curso_id",
     *     in="query",
     *     description="Id do curso",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          default="1"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function update(Request $request, $id)
    {
        $matricula = $this->matriculaService->update($request->all(), $id);
        return $this->retornoPadrao(null, 'Matricula atualizada.', 0, $this->transform($matricula, MatriculaTransformer::class));
    }

    /**
     * @OA\Delete(
     *   path="/matriculas/{id}",
     *   tags={"matriculas"},
     *   summary="Deletar um matricula",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Matricula id",
     *     required=false,
     *     @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function destroy($id)
    {
        $this->matriculaService->destroy($id);

        return $this->retornoPadrao(null, 'Matricula deletada.', 0, []);
    }

}
