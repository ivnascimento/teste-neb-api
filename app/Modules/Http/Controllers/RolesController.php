<?php

namespace App\Modules\Http\Controllers;

use App\Modules\Repositories\Contracts\RoleRepository;
use App\Modules\Services\RolesService;
use App\Modules\Transformers\RoleTransformer;
use Illuminate\Http\Request;
use App\Core\Http\Controllers\Controller;

class RolesController extends Controller
{

    private $rolesRepository;
    private $roleService;

    public function __construct(RoleRepository $rolesRepository, RolesService $roleService)
    {
        $this->rolesRepository = $rolesRepository;
        $this->roleService = $roleService;
    }

    /**
     * @OA\Get(
     *   path="/roles",
     *   tags={"roles"},
     *   summary="List roles",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="search",
     *     in="query",
     *     description="Search something(name of role).",
     *     required=false,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Data limit.",
     *     required=false,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="page",
     *     in="query",
     *     description="Page.",
     *     required=false,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="option",
     *     in="query",
     *     description="Option.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          enum={"1","0"},
     *          default="0"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function index(Request $request)
    {
        $option = $request->get('option', false);
        $request->request->remove('option');

        $retorno = $this->rolesRepository->paginate($request->limit ?? 15);
        return $this->verificarOptionERetornarDados($option, $retorno);
    }

    /**
     * @OA\Get(
     *   path="/roles/{id}",
     *   tags={"roles"},
     *   summary="Get a role",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Id of role",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="option",
     *     in="query",
     *     description="Option.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          enum={"1","0"},
     *          default="0"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=404, description="not found"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function show(Request $request, $id)
    {
        $option = $request->get('option', false);
        $request->request->remove('option');

        $retorno = $this->rolesRepository->find($id);
        return $this->verificarOptionERetornarDados($option, $retorno);
    }

    /**
     * @OA\Post(
     *   path="/roles",
     *   tags={"roles"},
     *   summary="Create a role",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="name",
     *     in="query",
     *     description="Name of role.",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="permission_ids[]",
     *     in="query",
     *     description="Permission ids.",
     *     required=true,
     *     @OA\Schema(
     *      type="array",
     *      collectionFormat="multi",
     *          @OA\Items(
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function store(Request $request)
    {
        $role = $this->roleService->store($request->all());
        $this->roleService->attachPermissions($request->permission_ids, $role->id);

        return $this->retornoPadrao(null, 'Grupo de usuário adicionado.', 0, $this->transform($role, RoleTransformer::class));
    }

    /**
     * @OA\Put(
     *   path="/roles/{id}",
     *   tags={"roles"},
     *   summary="Update a role",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Id of role",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="name",
     *     in="query",
     *     description="Name of role.",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="permission_ids[]",
     *     in="query",
     *     description="Permission ids.",
     *     required=true,
     *     @OA\Schema(
     *        type="array",
     *        @OA\Items(@OA\Schema(
     *            type="integer"
     *        ))
     *      ),
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function update(Request $request, $id)
    {
        $role = $this->roleService->update($request->all(), $id);
        $this->roleService->attachPermissions($request->permission_ids, $role->id);

        return $this->retornoPadrao(null, 'Grupo de usuário atualizado.', 0, $this->transform($role, RoleTransformer::class));
    }

    /**
     * @OA\Delete(
     *   path="/roles/{id}",
     *   tags={"roles"},
     *   summary="Delete a role",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Id of role",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function destroy($id)
    {
        $this->roleService->destroy($id);

        return $this->retornoPadrao(null, 'Grupo de usuário deletado.', 0, []);
    }

}
