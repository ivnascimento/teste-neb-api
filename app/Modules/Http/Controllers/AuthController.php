<?php

namespace App\Modules\Http\Controllers;

use App\Core\Support\Traits\Transformable;
use App\Modules\Transformers\UserTransformer;
use App\Modules\Validators\AuthValidator;
use Illuminate\Http\Request;
use App\Core\Http\Controllers\Controller;
use App\Modules\Entities\Usuario;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use Transformable;

    public $successStatus = 200;
    private $authValidator;

    public function __construct(AuthValidator $authValidator)
    {
        $this->authValidator = $authValidator;
    }


    /**
     * @OA\Post(
     *   path="/auth/login",
     *   tags={"login"},
     *   summary="Make a login of user",
     *   @OA\Parameter(
     *     name="login",
     *     in="query",
     *     description="username/email",
     *     required=true,
     *     @OA\Schema(
     *      type="string",
     *      default="adm@iury.com"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="senha",
     *     in="query",
     *     description="Password of user.",
     *     required=true,
     *     @OA\Schema(
     *      type="string",
     *      default="123456"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=401, description="not authorized"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function login(Request $request)
    {
        $this->authValidator->with($request->all())->passesOrFail(AuthValidator::RULE_AUTH);

        $user = Usuario::whereEmail($request->get('login'))->first();

        if ($user) {
            if (Hash::check($request->get('senha'), $user->senha)) {
                $token = $user->createToken('MyApp')->accessToken;
                $user = $this->transform($user, UserTransformer::class);
                if (stripos($_SERVER["REQUEST_URI"], '/api/v1/') === false)
                    $user['token'] = $token;
                else
                    $user['data']['token'] = $token;


                return $this->retornoPadrao(null, 'Autenticado com sucesso.', 0, $user);
            }
        }

        return $this->retornoPadrao(null, 'Não autenticado.', 1, []);
    }


    /**
     * @OA\Post(
     *   path="/auth/logout",
     *   tags={"login"},
     *   summary="Logout user",
     *   security={{"bearer": {}}},
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=401, description="not authorized"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return $this->retornoPadrao(null, 'Deslogado com sucesso.', 0, []);
    }
    
}
