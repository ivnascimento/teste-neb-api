<?php

namespace App\Modules\Http\Controllers;

use App\Modules\Repositories\Contracts\PermissionRepository;
use App\Modules\Services\PermissionsService;
use App\Modules\Transformers\PermissionTransformer;
use Illuminate\Http\Request;
use App\Core\Http\Controllers\Controller;

class PermissionsController extends Controller
{

    private $permissionRepository;
    private $permissionService;

    public function __construct(PermissionRepository $permissionRepository, PermissionsService $permissionService)
    {
        $this->permissionRepository = $permissionRepository;
        $this->permissionService = $permissionService;
    }

    /**
     * @OA\Get(
     *   path="/permissions",
     *   tags={"permissions"},
     *   summary="List permissions",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="search",
     *     in="query",
     *     description="Search something(name of role).",
     *     required=false,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Data limit.",
     *     required=false,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="page",
     *     in="query",
     *     description="Page.",
     *     required=false,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="option",
     *     in="query",
     *     description="Option.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          enum={"1","0"},
     *          default="0"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function index(Request $request)
    {
        $option = $request->get('option', false);
        $request->request->remove('option');

        $retorno = $this->permissionRepository->paginate($request->limit ?? 15);
        return $this->verificarOptionERetornarDados($option, $retorno);
    }

    /**
     * @OA\Get(
     *   path="/permissions/{id}",
     *   tags={"permissions"},
     *   summary="Get a permission",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Permission id",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="option",
     *     in="query",
     *     description="Option.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          enum={"1","0"},
     *          default="0"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=404, description="not found"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function show(Request $request, $id)
    {
        $option = $request->get('option', false);
        $request->request->remove('option');

        $retorno = $this->permissionRepository->find($id);
        return $this->verificarOptionERetornarDados($option, $retorno);
    }

    /**
     * @OA\Post(
     *   path="/permissions",
     *   tags={"permissions"},
     *   summary="Create a permission",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="name",
     *     in="query",
     *     description="Permission name.",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="readable_name",
     *     in="query",
     *     description="Readable permission name.",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function store(Request $request)
    {
        $permission = $this->permissionService->store($request->all());
        return $this->retornoPadrao(null, 'Permissão adicionada.', 0, $this->transform($permission, PermissionTransformer::class));
    }

    /**
     * @OA\Put(
     *   path="/permissions/{id}",
     *   tags={"permissions"},
     *   summary="Update a permission",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Permission id",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="name",
     *     in="query",
     *     description="Permission name.",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="readable_name",
     *     in="query",
     *     description="Readable permission name.",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function update(Request $request, $id)
    {
        $permission = $this->permissionService->update($request->all(), $id);
        return $this->retornoPadrao(null, 'Permissão atualizada.', 0, $this->transform($permission, PermissionTransformer::class));
    }

    /**
     * @OA\Delete(
     *   path="/permissions/{id}",
     *   tags={"permissions"},
     *   summary="Delete a permission",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Permission id",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function destroy($id)
    {
        $this->permissionService->destroy($id);

        return $this->retornoPadrao(null, 'Permissão deletada.', 0, []);
    }

}
