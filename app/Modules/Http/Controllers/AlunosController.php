<?php

namespace App\Modules\Http\Controllers;

use App\Modules\Repositories\Contracts\AlunoRepository;
use App\Modules\Services\AlunoService;
use App\Modules\Transformers\AlunoTransformer;
use Illuminate\Http\Request;
use App\Core\Http\Controllers\Controller;

class AlunosController extends Controller
{

    private $alunoRepository;
    private $alunoService;

    public function __construct(AlunoRepository $alunoRepository, AlunoService $alunoService)
    {
        $this->alunoRepository = $alunoRepository;
        $this->alunoService = $alunoService;
    }

    /**
     * @OA\Get(
     *   path="/alunos",
     *   tags={"alunos"},
     *   summary="Lista de alunos",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="search",
     *     in="query",
     *     description="Filtros (nome/email).",
     *     required=false,
     *     @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Data limit.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="page",
     *     in="query",
     *     description="Page.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="option",
     *     in="query",
     *     description="Option.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          enum={"1","0"},
     *          default="0"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function index(Request $request)
    {
        $option = $request->get('option', false);
        $request->request->remove('option');

        $retorno = $this->alunoRepository->paginate($request->get('limit', 15));
        return $this->verificarOptionERetornarDados($option, $retorno);
    }

     /**
     * @OA\Get(
     *   path="/alunos/{id}",
     *   tags={"alunos"},
     *   summary="Obter um aluno",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="id do aluno",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          default="1"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="option",
     *     in="query",
     *     description="Option.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          enum={"1","0"},
     *          default="0"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=404, description="not found"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function show(Request $request, $id)
    {
        $option = $request->get('option', false);
        $request->request->remove('option');

        $retorno = $this->alunoRepository->find($id);
        return $this->verificarOptionERetornarDados($option, $retorno);
    }

    /**
     * @OA\Post(
     *   path="/alunos",
     *   tags={"alunos"},
     *   summary="Criar um aluno",
     *   @OA\Parameter(
     *     name="nome",
     *     in="query",
     *     description="Nome completo.",
     *     required=true,
     *     @OA\Schema(
     *          type="string",
     *          default="Jose"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="cpf",
     *     in="query",
     *     description="CPF.",
     *     required=true,
     *     @OA\Schema(
     *          type="string",
     *          default="123.456.789-10"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="data_nascimento",
     *     in="query",
     *     description="Data de Nascimento (formato: Y-m-d)",
     *     required=true,
     *     @OA\Schema(
     *          type="string",
     *          format="Y-m-d",
     *          default="2000-01-01"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="email",
     *     in="query",
     *     description="Email.",
     *     required=true,
     *     @OA\Schema(
     *          type="string",
     *          default="teste@teste.com"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="telefone",
     *     in="query",
     *     description="Telefone.",
     *     required=true,
     *     @OA\Schema(
     *          type="string",
     *          default="62990001234"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function store(Request $request)
    {
        $aluno = $this->alunoService->store($request->all());
        return $this->retornoPadrao(null, 'Aluno adicionada.', 0, $this->transform($aluno, AlunoTransformer::class));
    }

    /**
     * @OA\Put(
     *   path="/alunos/{id}",
     *   tags={"alunos"},
     *   summary="Atualizar um aluno",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="nome",
     *     in="query",
     *     description="Nome completo.",
     *     required=false,
     *     @OA\Schema(
     *          type="string",
     *          default="Jose"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="cpf",
     *     in="query",
     *     description="CPF.",
     *     required=false,
     *     @OA\Schema(
     *          type="string",
     *          default="123.456.789-10"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="data_nascimento",
     *     in="query",
     *     description="Data de Nascimento (formato: Y-m-d)",
     *     required=false,
     *     @OA\Schema(
     *          type="string",
     *          format="Y-m-d",
     *          default="2000-01-01"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="email",
     *     in="query",
     *     description="Email.",
     *     required=false,
     *     @OA\Schema(
     *          type="string",
     *          default="teste@teste.com"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="telefone",
     *     in="query",
     *     description="Telefone.",
     *     required=false,
     *     @OA\Schema(
     *          type="string",
     *          default="62990001234"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function update(Request $request, $id)
    {
        $aluno = $this->alunoService->update($request->all(), $id);
        return $this->retornoPadrao(null, 'Aluno atualizada.', 0, $this->transform($aluno, AlunoTransformer::class));
    }

    /**
     * @OA\Delete(
     *   path="/alunos/{id}",
     *   tags={"alunos"},
     *   summary="Deletar um aluno",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Aluno id",
     *     required=false,
     *     @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function destroy($id)
    {
        $this->alunoService->destroy($id);

        return $this->retornoPadrao(null, 'Aluno deletada.', 0, []);
    }

}
