<?php

namespace App\Modules\Http\Controllers;

use App\Modules\Repositories\Contracts\CursoRepository;
use App\Modules\Services\CursoService;
use App\Modules\Transformers\CursoTransformer;
use Illuminate\Http\Request;
use App\Core\Http\Controllers\Controller;

class CursosController extends Controller
{

    private $cursoRepository;
    private $cursoService;

    public function __construct(CursoRepository $cursoRepository, CursoService $cursoService)
    {
        $this->cursoRepository = $cursoRepository;
        $this->cursoService = $cursoService;
    }

    /**
     * @OA\Get(
     *   path="/cursos",
     *   tags={"cursos"},
     *   summary="Lista de cursos",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="search",
     *     in="query",
     *     description="Filtros (nome).",
     *     required=false,
     *     @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Data limit.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="page",
     *     in="query",
     *     description="Page.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="option",
     *     in="query",
     *     description="Option.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          enum={"1","0"},
     *          default="0"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function index(Request $request)
    {
        $option = $request->get('option', false);
        $request->request->remove('option');

        $retorno = $this->cursoRepository->paginate($request->get('limit', 15));
        return $this->verificarOptionERetornarDados($option, $retorno);
    }

     /**
     * @OA\Get(
     *   path="/cursos/{id}",
     *   tags={"cursos"},
     *   summary="Obter um curso",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="id do curso",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          default="1"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="option",
     *     in="query",
     *     description="Option.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          enum={"1","0"},
     *          default="0"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=404, description="not found"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function show(Request $request, $id)
    {
        $option = $request->get('option', false);
        $request->request->remove('option');

        $retorno = $this->cursoRepository->find($id);
        return $this->verificarOptionERetornarDados($option, $retorno);
    }

    /**
     * @OA\Post(
     *   path="/cursos",
     *   tags={"cursos"},
     *   summary="Criar um curso",
     *   @OA\Parameter(
     *     name="nome",
     *     in="query",
     *     description="Nome",
     *     required=true,
     *     @OA\Schema(
     *          type="string",
     *          default="Administração"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function store(Request $request)
    {
        $curso = $this->cursoService->store($request->all());
        return $this->retornoPadrao(null, 'Curso adicionada.', 0, $this->transform($curso, CursoTransformer::class));
    }

    /**
     * @OA\Put(
     *   path="/cursos/{id}",
     *   tags={"cursos"},
     *   summary="Atualizar um curso",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="nome",
     *     in="query",
     *     description="Nome",
     *     required=false,
     *     @OA\Schema(
     *          type="string",
     *          default="Administração"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function update(Request $request, $id)
    {
        $curso = $this->cursoService->update($request->all(), $id);
        return $this->retornoPadrao(null, 'Curso atualizada.', 0, $this->transform($curso, CursoTransformer::class));
    }

    /**
     * @OA\Delete(
     *   path="/cursos/{id}",
     *   tags={"cursos"},
     *   summary="Deletar um curso",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Curso id",
     *     required=false,
     *     @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function destroy($id)
    {
        $this->cursoService->destroy($id);

        return $this->retornoPadrao(null, 'Curso deletada.', 0, []);
    }

}
