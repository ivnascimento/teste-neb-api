<?php

namespace App\Modules\Http\Controllers;

use App\Modules\Repositories\Contracts\UserRepository;
use App\Modules\Services\UsersService;
use App\Modules\Repositories\Criterias\ModelByFieldAndOperator;
use App\Modules\Transformers\UserTransformer;
use Illuminate\Http\Request;
use App\Core\Http\Controllers\Controller;
use Illuminate\Support\Carbon;

class UsuariosController extends Controller
{
    private $usuarioRepository;
    private $usersService;

    public function __construct(UserRepository $userRepository, UsersService $usersService)
    {
        $this->usuarioRepository = $userRepository;
        $this->usersService = $usersService;
    }

    /**
     * @OA\Get(
     *   path="/usuarios",
     *   tags={"usuarios"},
     *   summary="Lista de usuarios",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="search",
     *     in="query",
     *     description="Filtros (nome/email).",
     *     required=false,
     *     @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Data limit.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="page",
     *     in="query",
     *     description="Page.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="option",
     *     in="query",
     *     description="Option.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          enum={"1","0"},
     *          default="0"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function index(Request $request)
    {
        $option = $request->get('option', false);
        $request->request->remove('option');

        $usuarioRepository = $this->usuarioRepository->skipPresenter();

        $usuarios = $usuarioRepository->skipPresenter()->paginate($request->limit ?? 15);
        $retorno = $this->transform($usuarios, UserTransformer::class);
        return $this->verificarOptionERetornarDados($option, $retorno);
    }

    /**
     * @OA\Get(
     *   path="/usuarios/{id}",
     *   tags={"usuarios"},
     *   summary="Obter um usuario",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="id do usuário",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          default="1"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="option",
     *     in="query",
     *     description="Option.",
     *     required=false,
     *     @OA\Schema(
     *          type="integer",
     *          enum={"1","0"},
     *          default="0"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=404, description="not found"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function show(Request $request, $id)
    {
        $option = $request->get('option', false);
        $request->request->remove('option');

        $retorno = $this->usuarioRepository->find($id);
        return $this->verificarOptionERetornarDados($option, $retorno);
    }


    /**
     * @OA\Post(
     *   path="/usuarios",
     *   tags={"usuarios"},
     *   summary="Criar um usuario",
     *   @OA\Parameter(
     *     name="nome",
     *     in="query",
     *     description="Nome completo.",
     *     required=false,
     *     @OA\Schema(
     *          type="string",
     *          default="superuser"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="email",
     *     in="query",
     *     description="Email.",
     *     required=false,
     *     @OA\Schema(
     *          type="string",
     *          default="teste@teste.com"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="senha",
     *     in="query",
     *     description="Senha.",
     *     required=false,
     *     @OA\Schema(
     *          type="string",
     *          default="123456"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="situacao",
     *     in="query",
     *     description="Situação.",
     *     required=false,
     *     @OA\Schema(
     *          type="string",
     *          enum={"Ativo", "Bloqueado"},
     *          default="Ativo"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="roles[]",
     *     in="query",
     *     description="Role ids.",
     *     required=false,
     *     @OA\Schema(
     *          type="array",
     *          collectionFormat="multi",
     *          @OA\Items(type="integer")
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function store(Request $request)
    {
        $usuario = $this->usersService->store($request->all());
        $token = $usuario->createToken('MyApp')->accessToken;
        $usuario = $this->transform($usuario, UserTransformer::class);
        if (stripos($_SERVER["REQUEST_URI"], '/api/v1/') === false)
            $usuario['token'] = $token;
        else
            $usuario['data']['token'] = $token;

        return $this->retornoPadrao(null, 'Usuário adicionado.', 0, $usuario);
    }

    /**
     * @OA\Put(
     *   path="/usuarios/{id}",
     *   tags={"usuarios"},
     *   summary="Atualizar um usuario",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="nome",
     *     in="query",
     *     description="Nome completo.",
     *     required=false,
     *     @OA\Schema(
     *          type="string",
     *          default="superuser"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="email",
     *     in="query",
     *     description="Email.",
     *     required=false,
     *     @OA\Schema(
     *          type="string",
     *          default="teste@teste.com"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="senha",
     *     in="query",
     *     description="Senha.",
     *     required=false,
     *     @OA\Schema(
     *          type="string",
     *          default="123456"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="situacao",
     *     in="query",
     *     description="Situação.",
     *     required=false,
     *     @OA\Schema(
     *          type="string",
     *          enum={"Ativo", "Bloqueado"},
     *          default="Ativo"
     *      )
     *   ),
     *   @OA\Parameter(
     *     name="roles[]",
     *     in="query",
     *     description="Role ids.",
     *     required=false,
     *     @OA\Schema(
     *          type="array",
     *          collectionFormat="multi",
     *          @OA\Items(type="integer")
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function update(Request $request, $id)
    {
        $usuario = $this->usersService->update($request->all(), $id);

        return $this->retornoPadrao(null, 'Usuário atualizado.', 0, $this->transform($usuario, UserTransformer::class));
    }

    /**
     * @OA\Delete(
     *   path="/usuarios/{id}",
     *   tags={"usuarios"},
     *   summary="Deletar um usuario",
     *   security={{"bearer": {}}},
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Usuario id",
     *     required=false,
     *     @OA\Schema(
     *          type="integer"
     *      )
     *   ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error")
     * )
     *
     */
    public function destroy($id)
    {
        $this->usersService->destroy($id);

        return $this->retornoPadrao(null, 'Usuário deletado.', 0, []);
    }

}
