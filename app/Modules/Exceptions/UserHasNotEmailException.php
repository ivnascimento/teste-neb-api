<?php

namespace App\Modules\Exceptions;

use App\Core\Exceptions\AbstractException;

class UserHasNotEmailException extends AbstractException
{
    public function __construct()
    {
        parent::__construct(['error' => 'Este usuário não possui email cadastrado']);
    }
}
