<?php

namespace App\Modules\Exceptions;

use App\Core\Exceptions\AbstractException;

class UsuarioNaoEncontradoException extends AbstractException
{
    public function __construct()
    {
        parent::__construct(['error' => 'Usuário não encontrado.']);
    }
}
