<?php

namespace App\Modules\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class UserValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'nome' => 'required|string|max:254',
            'email' => 'required|email|unique:mysql-api.usuarios,email,NULL,id,deleted_at,NULL',
            'senha' => 'required|string|min:6',
            'situacao' => 'required|string|in:Ativo,Bloqueado'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'nome' => 'filled|string|max:254',
            'email' => 'filled|email|unique:mysql-api.usuarios,email,NULL,id,deleted_at,NULL',
            'senha' => 'filled|string|min:6',
            'situacao' => 'filled|string|in:Ativo,Bloqueado,Liberado'
        ],
    ];
}
