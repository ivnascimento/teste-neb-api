<?php

namespace App\Modules\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class PermissionValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|string|unique:mysql-usuario.permissions,name',
            'readable_name' => 'required|string'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required|string|unique:mysql-usuario.permissions,name',
            'readable_name' => 'required|string'
        ],
    ];

    protected $attributes = [
        'name' => 'nome da permissão',
        'readable_name' => 'nome descritivo da permissão'
    ];
}
