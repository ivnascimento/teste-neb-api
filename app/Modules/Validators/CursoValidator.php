<?php

namespace App\Modules\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class CursoValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'nome' => 'required|string|max:254'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'nome' => 'filled|string|max:254'
        ],
    ];
}
