<?php

namespace App\Modules\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class AlunoValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'nome' => 'required|string|max:254',
            'cpf' => 'required|string|unique:mysql-api.alunos,cpf,NULL,id,deleted_at,NULL',
            'data_nascimento' => 'required|date',
            'email' => 'required|email|unique:mysql-api.alunos,email,NULL,id,deleted_at,NULL',
            'telefone' => 'required|string'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'nome' => 'filled|string|max:254',
            'cpf' => 'filled|string|unique:mysql-api.alunos,cpf,NULL,id,deleted_at,NULL',
            'data_nascimento' => 'filled|date',
            'email' => 'filled|email|unique:mysql-api.alunos,email,NULL,id,deleted_at,NULL',
            'telefone' => 'filled|string'
        ],
    ];
}
