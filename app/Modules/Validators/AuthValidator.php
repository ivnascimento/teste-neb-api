<?php

namespace App\Modules\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class AuthValidator extends LaravelValidator
{
    const RULE_AUTH = 'auth';

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
 
        ],
        ValidatorInterface::RULE_UPDATE => [
        
        ],
        self::RULE_AUTH => [
            'senha' => 'required|string|min:6',
            'login' => 'required|string',
        ]
    ];
}
