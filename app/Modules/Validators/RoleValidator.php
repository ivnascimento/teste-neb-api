<?php

namespace App\Modules\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class RoleValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|string|unique:roles,name',
            'permission_ids' => 'required|array',
            'permission_ids.*' => 'required|integer|exists:mysql-usuario.permissions,id',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'string|unique:roles,name',
            'permission_ids' => 'required|array',
            'permission_ids.*' => 'required|integer|exists:mysql-usuario.permissions,id',
        ]
    ];

    protected $attributes = [
        'name' => 'nome da regra',
        'permission_ids'   => 'id das permissões'
    ];
}
