<?php

namespace App\Modules\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class MatriculaValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'data_matricula' => 'required|date',
            'status' => 'required|string|in:A,B,C,D,F,T',
            'aluno_id' => 'required|integer|exists:mysql-api.alunos,id,deleted_at,NULL',
            'curso_id' => 'required|integer|exists:mysql-api.cursos,id,deleted_at,NULL',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'data_matricula' => 'filled|date',
            'status' => 'filled|string|in:A,B,C,D,F,T',
            'aluno_id' => 'filled|integer|exists:mysql-api.alunos,id,deleted_at,NULL',
            'curso_id' => 'filled|integer|exists:mysql-api.cursos,id,deleted_at,NULL',
        ],
    ];
}