<?php

namespace App\Modules\Transformers;

use League\Fractal\TransformerAbstract;
use App\Modules\Entities\Usuario;


class UserTransformer extends TransformerAbstract
{

    protected $defaultIncludes = [
        'roles'
    ];

    public function transform(Usuario $model)
    {
        return [
            'id' => $model->id,
            'nome' => $model->nome,
            'email' => $model->email,
            'situacao' => $model->situacao,
            'created_at' => $model->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $model->updated_at->format('Y-m-d H:i:s'),
        ];
    }

    public function includeRoles(Usuario $model)
    {
        return $this->collection($model->roles, new RoleTransformer());
    }
}
