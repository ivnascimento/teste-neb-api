<?php

namespace App\Modules\Transformers;

use Artesaos\Defender\Permission;
use League\Fractal\TransformerAbstract;

class PermissionTransformer extends TransformerAbstract
{
    public function transform(Permission $model)
    {
        return [
            'id' => $model->id,
            'name' => $model->name,
            'readable_name' => $model->readable_name,
        ];
    }
}
