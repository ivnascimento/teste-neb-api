<?php

namespace App\Modules\Transformers;

use Artesaos\Defender\Permission;
use Artesaos\Defender\Role;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'permissions'
    ];

    public function transform(Role $role)
    {
        return [
            'id' => $role->id,
            'name' => $role->name
        ];
    }

    public function includePermissions(Role $role)
    {
        $rolePermissions = $role->permissions;
        return $this->collection($rolePermissions, new PermissionTransformer());
    }
}
