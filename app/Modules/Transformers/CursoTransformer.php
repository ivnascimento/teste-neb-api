<?php

namespace App\Modules\Transformers;

use League\Fractal\TransformerAbstract;
use App\Modules\Entities\Curso;


class CursoTransformer extends TransformerAbstract
{

    public function transform(Curso $model)
    {
        return [
            'id' => $model->id,
            'nome' => $model->nome,
            'created_at' => $model->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $model->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}
