<?php

namespace App\Modules\Transformers;

use League\Fractal\TransformerAbstract;
use App\Modules\Entities\Matricula;


class MatriculaTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'alunos',
        'cursos'
    ];

    public function transform(Matricula $matricula)
    {
        return [
            'id' => $matricula->id,          
            'data_matricula' => $matricula->data_matricula,  
            'status' => $matricula->status,  
            'aluno_id' => $matricula->aluno_id,  
            'curso_id' => $matricula->curso_id,  
            'created_at' => $matricula->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $matricula->updated_at->format('Y-m-d H:i:s'),
        ];
    }

    public function includeAlunos(Matricula $matricula)
    {
        if ($matricula->alunos) {
            return $this->item($matricula->alunos, new AlunoTransformer());
        }
    }

    public function includeCursos(Matricula $matricula)
    {
        if ($matricula->cursos) {
            return $this->item($matricula->cursos, new CursoTransformer());
        }
    }
}
