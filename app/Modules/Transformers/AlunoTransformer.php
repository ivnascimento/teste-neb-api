<?php

namespace App\Modules\Transformers;

use League\Fractal\TransformerAbstract;
use App\Modules\Entities\Aluno;


class AlunoTransformer extends TransformerAbstract
{

    public function transform(Aluno $model)
    {
        return [
            'id' => $model->id,
            'nome' => $model->nome,
            'cpf' => $model->cpf,
            'data_nascimento' => $model->data_nascimento,
            'email' => $model->email,
            'telefone' => $model->telefone,
            'created_at' => $model->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $model->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}
