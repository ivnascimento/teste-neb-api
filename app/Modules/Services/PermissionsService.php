<?php

namespace App\Modules\Services;

use App\Modules\Repositories\Contracts\PermissionRepository;
use App\Modules\Validators\PermissionValidator;
use Prettus\Validator\Contracts\ValidatorInterface;

class PermissionsService
{
    private $permissionRepository;
    private $permissionValidator;

    public function __construct(PermissionRepository $permissionRepository, PermissionValidator $permissionValidator)
    {
        $this->permissionRepository = $permissionRepository;
        $this->permissionValidator = $permissionValidator;
    }

    public function store(array $request)
    {
        $this->permissionValidator->with($request)->passesOrFail(ValidatorInterface::RULE_CREATE);
        $permission = $this->permissionRepository->skipPresenter()->create($request);

        return $permission;
    }

    public function update(array $request, $permissionId)
    {
        $this->permissionValidator->with($request)->setId($permissionId)->passesOrFail(ValidatorInterface::RULE_UPDATE);
        $permission = $this->permissionRepository->skipPresenter()->update($request, $permissionId);

        return $permission;
    }

    public function destroy($permissionId)
    {
        $this->permissionRepository->skipPresenter()->delete($permissionId);
    }
}
