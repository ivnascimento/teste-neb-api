<?php

namespace App\Modules\Services;

use App\Modules\Repositories\Contracts\RoleRepository;
use App\Modules\Validators\RoleValidator;
use Artesaos\Defender\Facades\Defender;
use Prettus\Validator\Contracts\ValidatorInterface;

class RolesService
{
    private $roleRepository;
    private $roleValidator;

    public function __construct(RoleRepository $roleRepository, RoleValidator $roleValidator)
    {
        $this->roleRepository = $roleRepository;
        $this->roleValidator = $roleValidator;
    }

    public function store(array $request)
    {
        $this->roleValidator->with($request)->passesOrFail(ValidatorInterface::RULE_CREATE);
        $role = $this->roleRepository->skipPresenter()->create($request);

        return $role;
    }

    public function update(array $request, $roleId)
    {
        $this->roleValidator->with($request)->setId($roleId)->passesOrFail(ValidatorInterface::RULE_UPDATE);
        $role = $this->roleRepository->skipPresenter()->update($request, $roleId);

        return $role;
    }

    public function destroy($roleId)
    {
        $this->roleRepository->skipPresenter()->delete($roleId);
    }

    public function attachPermissions(array $permissionIds, $roleId)
    {

        $role = Defender::findRoleById($roleId);

        $this->detachAllPermissionsFromRole($roleId);

        $role->attachPermission($permissionIds);

        $role = Defender::findRoleById($roleId);

        return $role;
    }

    private function detachAllPermissionsFromRole($roleId)
    {
        $role = Defender::findRoleById($roleId);
        $rolePermissions = $this->getRolePermissionIds($roleId);

        if(!empty($rolePermissions)){
            $role->detachPermission($rolePermissions);
        }

        return true;
    }

    private function getRolePermissionIds($roleId)
    {
        $role = Defender::findRoleById($roleId);

        $rolePermissions = $role->permissions;
        $permissions = [];

        foreach ($rolePermissions as $rolePermission) {
            $permissions[] = $rolePermission->id;
        }

        return $permissions;
    }

}
