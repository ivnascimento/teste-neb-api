<?php

namespace App\Modules\Services;

use App\Modules\Entities\Usuario;
use App\Modules\Repositories\Contracts\UserRepository;
use App\Modules\Validators\UserValidator;
use Artesaos\Defender\Facades\Defender;
use Illuminate\Support\Facades\Hash;
use Prettus\Validator\Contracts\ValidatorInterface;

class UsersService
{
    private $userRepository;
    private $userValidator;

    public function __construct(
        UserRepository $userRepository,
        UserValidator $userValidator
    )
    {
        $this->userRepository = $userRepository;
        $this->userValidator = $userValidator;
    }

    public function store(array $request)
    {
        $request = $this->mutarRequest($request);

        $this->userValidator->with($request)->passesOrFail(ValidatorInterface::RULE_CREATE);

        if (array_key_exists('senha', $request)) {
            $usuSenha = $request['senha'];
            $request['senha'] = Hash::make($usuSenha);
        }

        if (array_key_exists('email', $request)) {
            $userCriadoSite = Usuario::whereEmail($request['email'])->first();

            if ($userCriadoSite) {
                $user = $this->userRepository->skipPresenter()->update($request, $userCriadoSite->id);
            } else {
                $user = $this->userRepository->skipPresenter()->create($request);
            }
        } else {
            $user = $this->userRepository->skipPresenter()->create($request);
        }

        if (array_key_exists('roles', $request)) {
            $this->attachRoles($request['roles'], $user->id);
        }

        return $user->refresh();
    }

    public function update(array $request, $userId)
    {
        $request = $this->mutarRequest($request);
        $this->userValidator->with($request)->setId($userId)->passesOrFail(ValidatorInterface::RULE_UPDATE);

        if (array_key_exists('senha', $request)) {
            $usuSenha = $request['senha'];
            $request['senha'] = Hash::make($usuSenha);
        }

        $user = $this->userRepository->skipPresenter()->update($request, $userId);

        if (array_key_exists('roles', $request)) {
            $this->detachAllRolesFromUser($user->id);
            $this->attachRoles($request['roles'], $user->id);
        }

        return $user->refresh();
    }

    private function mutarRequest(array $request)
    {
        // Retirar espacos
        foreach (['email'] as $campo) {
            if (array_key_exists($campo, $request)) {
                $request[$campo] = str_replace(" ", "", $request[$campo]);
            }
        }

        return $request;
    }

    public function destroy($userId)
    {
        return $this->userRepository->skipPresenter()->delete($userId);
    }

    public function attachRoles(array $roleIds, $userId)
    {
        $roleIds = array_unique($roleIds);
        $user = $this->userRepository->skipPresenter()->find($userId);

        $this->detachAllRolesFromUser($userId);

        foreach ($roleIds as $roleId) {
            $role = Defender::findRoleById($roleId);
            $user->attachRole($role);
        }

        return $user->refresh();
    }

    public function detachAllRolesFromUser($userId)
    {
        $user = Usuario::find($userId);

        foreach ($user->roles as $role) {
            $user->detachRole($role);
        }

        return true;
    }
}
