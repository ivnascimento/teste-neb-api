<?php

namespace App\Modules\Services;

use App\Modules\Repositories\Contracts\AlunoRepository;
use App\Modules\Validators\AlunoValidator;
use Prettus\Validator\Contracts\ValidatorInterface;

class AlunoService
{
    private $alunoRepository;
    private $alunoValidator;

    public function __construct(AlunoRepository $alunoRepository, AlunoValidator $alunoValidator)
    {
        $this->alunoRepository = $alunoRepository;
        $this->alunoValidator = $alunoValidator;
    }

    public function store(array $request)
    {
        $request = $this->mutarRequest($request);
        $this->alunoValidator->with($request)->passesOrFail(ValidatorInterface::RULE_CREATE);
        return $this->alunoRepository->skipPresenter()->create($request);
    }

    public function update(array $request, $id)
    {
        $request = $this->mutarRequest($request);
        $this->alunoValidator->with($request)->setId($id)->passesOrFail(ValidatorInterface::RULE_UPDATE);
        return $this->alunoRepository->skipPresenter()->update($request, $id);
    }

    private function mutarRequest(array $request)
    {
        //retirar caracteres especiais do telefone
        foreach (['telefone', 'cpf'] as $campo) {
            if (array_key_exists($campo, $request)) {
                $request[$campo] = preg_replace("/[^0-9]/", "", $request[$campo]);
            }
        }

        // Retirar espacos
        foreach (['email'] as $campo) {
            if (array_key_exists($campo, $request)) {
                $request[$campo] = str_replace(" ", "", $request[$campo]);
            }
        }

        return $request;
    }

    public function destroy($id)
    {
        return $this->alunoRepository->skipPresenter()->delete($id);
    }
}
