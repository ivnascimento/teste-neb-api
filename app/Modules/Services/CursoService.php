<?php

namespace App\Modules\Services;

use App\Modules\Repositories\Contracts\CursoRepository;
use App\Modules\Validators\CursoValidator;
use Prettus\Validator\Contracts\ValidatorInterface;

class CursoService
{
    private $cursoRepository;
    private $cursoValidator;

    public function __construct(CursoRepository $cursoRepository, CursoValidator $cursoValidator)
    {
        $this->cursoRepository = $cursoRepository;
        $this->cursoValidator = $cursoValidator;
    }

    public function store(array $request)
    {
        $this->cursoValidator->with($request)->passesOrFail(ValidatorInterface::RULE_CREATE);
        return $this->cursoRepository->skipPresenter()->create($request);
    }

    public function update(array $request, $id)
    {
        $this->cursoValidator->with($request)->setId($id)->passesOrFail(ValidatorInterface::RULE_UPDATE);
        return $this->cursoRepository->skipPresenter()->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->cursoRepository->skipPresenter()->delete($id);
    }
}
