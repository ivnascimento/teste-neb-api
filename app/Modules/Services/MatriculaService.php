<?php

namespace App\Modules\Services;

use App\Modules\Repositories\Contracts\MatriculaRepository;
use App\Modules\Validators\MatriculaValidator;
use Prettus\Validator\Contracts\ValidatorInterface;

class MatriculaService
{
    private $matriculaRepository;
    private $matriculaValidator;

    public function __construct(MatriculaRepository $matriculaRepository, MatriculaValidator $matriculaValidator)
    {
        $this->matriculaRepository = $matriculaRepository;
        $this->matriculaValidator = $matriculaValidator;
    }

    public function store(array $request)
    {
        $this->matriculaValidator->with($request)->passesOrFail(ValidatorInterface::RULE_CREATE);
        return $this->matriculaRepository->skipPresenter()->create($request);
    }

    public function update(array $request, $id)
    {
        $this->matriculaValidator->with($request)->setId($id)->passesOrFail(ValidatorInterface::RULE_UPDATE);
        return $this->matriculaRepository->skipPresenter()->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->matriculaRepository->skipPresenter()->delete($id);
    }
}
