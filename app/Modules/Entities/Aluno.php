<?php

namespace App\Modules\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class Aluno extends Model
{
    use SoftDeletes, Userstamps;

    protected $table = 'alunos';
 
    protected $fillable = [
        'nome',
        'cpf',
        'data_nascimento',
        'email',
        'telefone'
    ];

    protected $date = [
        'created_by',
        'updated_by',
        'deleted_by'
    ];

}
