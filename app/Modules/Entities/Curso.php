<?php

namespace App\Modules\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class Curso extends Model
{
    use SoftDeletes, Userstamps;

    protected $table = 'cursos';
 
    protected $fillable = [
        'nome'
    ];

    protected $date = [
        'created_by',
        'updated_by',
        'deleted_by'
    ];

}
