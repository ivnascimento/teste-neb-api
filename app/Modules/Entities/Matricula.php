<?php

namespace App\Modules\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class Matricula extends Model
{
    use SoftDeletes, Userstamps;

    protected $table = 'matriculas';
 
    protected $fillable = [
        'data_matricula',
        'status',
        'aluno_id',
        'curso_id'
    ];

    protected $casts = [
        'aluno_id',
        'curso_id'
    ];

    protected $date = [
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    public function alunos() {
        return $this->hasOne(Aluno::class, 'id', 'aluno_id');
    }

    public function cursos() {
        return $this->hasOne(Curso::class, 'id', 'curso_id');
    }

}
