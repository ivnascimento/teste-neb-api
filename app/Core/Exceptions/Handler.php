<?php

namespace App\Core\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Prettus\Validator\Exceptions\ValidatorException;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{

    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }

    protected function prepareJsonResponse($request, Exception $exception)
    {
        if ($exception instanceof ValidatorException) {
            return response()->json([
                'erro'  => 1,
                'exception'  => get_class($exception),
                'md5'=>null,
                'msg'=> 'Erro Validação',
                'dados' => $exception->getMessageBag(),
            ], 400);
        }

        if ($exception instanceof NotFoundHttpException) {
            return response()->json([
                'erro'  => 1,
                'md5'=>null,
                'exception'  => get_class($exception),
                'msg'    => 'O recurso que você está tentando acessar não foi encontrado.',
                'dados'      => $this->getTrace($exception)
            ], 404);

        }

        $exception = FlattenException::create($exception);

        if (config('app.debug')) {
            $message = $exception->getMessage();
        } else {
            $message = Response::$statusTexts[$exception->getStatusCode()];
        }

        return response()->json([
            'erro'  => 1,
            'exception'  => $exception->getStatusCode(). ' - '.$exception->getClass(),
            'md5'=>null,
            'msg'=> 'Erro 400 - ' . $exception->getStatusCode() . ' - '. $message,
            'dados' => $exception->getTrace($exception),
        ], 400);
    }

    private function getTrace($exception)
    {
        if (config('app.debug')) {
            return 'file: ' . $exception->getFile() . ' line: ' . $exception->getLine();
        }

        return null;
    }
}
