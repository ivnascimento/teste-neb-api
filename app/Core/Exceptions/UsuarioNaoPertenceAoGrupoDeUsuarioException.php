<?php

namespace App\Core\Exceptions;

class UsuarioNaoPertenceAoGrupoDeUsuarioException extends AbstractException
{
    public function __construct($message)
    {
        parent::__construct(['error' => $message]);
    }
}
