<?php
namespace App\Core\Http\Middleware;

use Closure;
class AlwaysExpectsJson
{
    public function handle($request, Closure $next)
    {
        if(stripos($_SERVER["REQUEST_URI"],'/api/doc')===false){
            $request->headers->add(['Accept' => 'application/json']);
        }
        return $next($request);
    }
}
