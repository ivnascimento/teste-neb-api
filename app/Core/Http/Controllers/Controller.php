<?php

namespace App\Core\Http\Controllers;

use App\Core\Support\Traits\Transformable;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use stdClass;

/**
 * @OA\Info(
 *  title="API AdmSchool",
 *  version="1.0",
 *  description="API points AdmSchool.",
 *  @OA\Contact(name="Iury Nascimento",email="i.v.nascimento.ti@gmail.com")
 * )
 */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, Transformable;

    public function verificarOptionERetornarDados($option, $retorno, $msg = 'OK', $erro = 0)
    {
        $md5 = md5(json_encode($retorno));
        if ($option) {
            return $this->retornoPadrao($md5, null, null, env('APP_ENV') != 'production' ? $retorno : null, true);
        }
        return $this->retornoPadrao($md5, $msg, $erro, $retorno);
    }
   
    public function retornoPadrao($md5, $msg, $erro, $dados, $default = true)
    {
    
        $extras=[];
        
        if (stripos($_SERVER["REQUEST_URI"], '/api/v1/')===false) {
        if (is_array($dados)) {
            if (count($dados) == 0) {
                $dados = null;
            }
            if(isset($dados["data"]))
            if(count($dados["data"])==0)
                $dados=null;
        }

            $retornoN=[];
            $retirarDoRetornoAddExtra=['meta','pagination'];

            if(is_array($dados))
            foreach($retirarDoRetornoAddExtra as $chaveRetirar){
                    if(array_key_exists ($chaveRetirar, $dados)){
                        $extras[$chaveRetirar]=$dados[$chaveRetirar];
                        unset($dados[$chaveRetirar]);
                    }
            }

            
            if (isset($dados["data"])) {
                $retornoN=$dados["data"];
            }else{
                $retornoN=$dados;
                if (is_array($dados)) {
                    if (count($dados) == 0) {
                        $retornoN= null;
                    }elseif (@array_keys($dados)[0]===0) {
                        $retornoN=array_values($dados);
                    }
                }
            }

            $dados=$retornoN;
        }

        if ($default) {
            return array_merge(['md5' => $md5, 'msg' => $msg, 'erro' => $erro, 'dados' => $dados],$extras);
        }

        return ['md5' => $md5];
    }
}
