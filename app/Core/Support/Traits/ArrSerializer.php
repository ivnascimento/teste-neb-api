<?php

namespace App\Core\Support\Traits;
use  League\Fractal\Serializer\ArraySerializer;

class ArrSerializer extends ArraySerializer
{

    public function collection($resourceKey, array $data)
    {
        return $data;
    }

    public function item($resourceKey, array $data)
    {        
        return $data;
    }

    public function null()
    {
        return null;
    }
}
