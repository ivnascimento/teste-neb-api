<?php

use Illuminate\Database\Seeder;
use Artesaos\Defender\Facades\Defender;

class RolesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Defender::findRole('superuser');
        if (!$role) {
            Defender::createRole('superuser');
        }
       
    }
}
