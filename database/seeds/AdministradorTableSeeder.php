<?php

use Illuminate\Database\Seeder;
use App\Modules\Entities\Usuario;
use Artesaos\Defender\Facades\Defender;

class AdministradorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuario = Usuario::where('email', 'adm@iury.com')->first();
        if (!$usuario) {
            $usuario = Usuario::create([
                'nome' => 'Iury Nascimento',
                'email' => 'adm@iury.com',
                'senha' => bcrypt('123456'),
                'situacao' => 'Ativo',
            ]);

            $role = Defender::findRole('superuser');

            $usuario->attachRole($role);
        }
    }
}
