<?php

use Illuminate\Database\Seeder;
use Artesaos\Defender\Facades\Defender;

class PermissionsTableSeeder extends Seeder
{

    public function run()
    {
        $permissoes = [];

        $permissoes['aluno.index'] = ['nome' => 'Index Aluno', 'role' => ['superuser']];
        $permissoes['aluno.show'] = ['nome' => 'Show Aluno', 'role' => ['superuser']];
        $permissoes['aluno.store'] = ['nome' => 'Store Aluno', 'role' => ['superuser']];
        $permissoes['aluno.update'] = ['nome' => 'Update Aluno', 'role' => ['superuser']];
        $permissoes['aluno.delete'] = ['nome' => 'Delete Aluno', 'role' => ['superuser']];

        $permissoes['matricula.index'] = ['nome' => 'Index Matricula', 'role' => ['superuser']];
        $permissoes['matricula.show'] = ['nome' => 'Show Matricula', 'role' => ['superuser']];
        $permissoes['matricula.store'] = ['nome' => 'Store Matricula', 'role' => ['superuser']];
        $permissoes['matricula.update'] = ['nome' => 'Update Matricula', 'role' => ['superuser']];
        $permissoes['matricula.delete'] = ['nome' => 'Delete Matricula', 'role' => ['superuser']];

        $permissoes['curso.index'] = ['nome' => 'Index Curso', 'role' => ['superuser']];
        $permissoes['curso.show'] = ['nome' => 'Show Curso', 'role' => ['superuser']];
        $permissoes['curso.store'] = ['nome' => 'Store Curso', 'role' => ['superuser']];
        $permissoes['curso.update'] = ['nome' => 'Update Curso', 'role' => ['superuser']];
        $permissoes['curso.delete'] = ['nome' => 'Delete Curso', 'role' => ['superuser']];

        $permissoes['permissions.index'] = ['nome' => 'Index Permissions', 'role' => ['superuser']];
        $permissoes['permissions.show'] = ['nome' => 'Show Permissions', 'role' => ['superuser']];
        $permissoes['permissions.store'] = ['nome' => 'Store Permissions', 'role' => ['superuser']];
        $permissoes['permissions.update'] = ['nome' => 'Update Permissions', 'role' => ['superuser']];
        $permissoes['permissions.delete'] = ['nome' => 'Delete Permissions', 'role' => ['superuser']];

        $permissoes['roles.index'] = ['nome' => 'Index Roles', 'role' => ['superuser']];
        $permissoes['roles.show'] = ['nome' => 'Show Roles', 'role' => ['superuser']];
        $permissoes['roles.store'] = ['nome' => 'Store Roles', 'role' => ['superuser']];
        $permissoes['roles.update'] = ['nome' => 'Update Roles', 'role' => ['superuser']];
        $permissoes['roles.delete'] = ['nome' => 'Delete Roles', 'role' => ['superuser']];

        $permissoes['usuario.index'] = ['nome' => 'Index Usuario', 'role' => ['superuser']];
        $permissoes['usuario.show'] = ['nome' => 'Show Usuario', 'role' => ['superuser']];
        $permissoes['usuario.store'] = ['nome' => 'Store Usuario', 'role' => ['superuser']];
        $permissoes['usuario.update'] = ['nome' => 'Update Usuario', 'role' => ['superuser']];
        $permissoes['usuario.delete'] = ['nome' => 'Delete Usuario', 'role' => ['superuser']];

        foreach ($permissoes as $chave => $permissao) {
            $per = Defender::findPermission($chave);
            if (!$per) {
                Defender::createPermission($chave, $permissao["nome"]);
                $per = Defender::findPermission($chave);
            }

            if (is_array($permissao["role"]))
                foreach ($permissao["role"] as $roleName) {
                    $role = Defender::findRole($roleName);

                    if (!$role) {
                        $role = Defender::createRole($roleName);
                    }

                    $role->detachPermission([$per->id]);
                    $role->attachPermission([$per->id]);
                }

        }
    }
}
