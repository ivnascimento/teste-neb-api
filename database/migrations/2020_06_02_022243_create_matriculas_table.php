<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatriculasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('matriculas', function(Blueprint $table) {
			$table->increments('id');
			$table->date('data_matricula');
			$table->enum('status', ['A', 'B', 'C', 'D', 'F', 'T']);
			$table->unsignedBigInteger('aluno_id');
			$table->unsignedBigInteger('curso_id');

            $table->softDeletes();
			$table->timestamps();
			$table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('matriculas');
	}
}
