<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlunosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alunos', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nome');
			$table->string('cpf')->unique();
			$table->date('data_nascimento')->nullable()->default(null);
			$table->string('email')->unique();
			$table->string('telefone')->nullable()->default(null);

            $table->softDeletes();
			$table->timestamps();
			$table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('alunos');
	}
}
