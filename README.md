# API

### É necessario ter docker instalado!

## Instalação
```
./instalar.sh
```

### Execução
```
docker-compose up -d
```

### Documentação da Api
```
http://localhost:81/
```

### Login
```
login: adm@iury.com
senha: 123456
```