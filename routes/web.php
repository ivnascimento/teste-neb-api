<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

Route::any('/', function () {
    return redirect()->to('/api/doc');
});

Route::any('/api/documentation', function () {
    return redirect()->to('/api/doc');
});