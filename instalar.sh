#!/bin/bash

APP_NAME="Api"

docker-compose up -d

if [ -f composer.json ]; then
    docker exec -it app-$APP_NAME composer config -g secure-http false

    echo "Vou instalar o php composer agora... "
    docker exec -it app-$APP_NAME composer install
fi 


if [ -f artisan ]; then
    
    docker exec -it app-$APP_NAME  php artisan config:clear
    docker exec -it app-$APP_NAME  php artisan config:cache
    
    echo "Rodando os migrates"
    docker exec -it app-$APP_NAME php artisan migrate --force

    echo "Rodando os Seedsss"
    docker exec -it app-$APP_NAME composer dump
    docker exec -it app-$APP_NAME php artisan db:seed --force

    docker exec -it app-$APP_NAME php artisan passport:install --force

    echo Publishing vendor
    docker exec -it app-$APP_NAME php artisan vendor:publish --provider "L5Swagger\L5SwaggerServiceProvider"

    docker exec -it app-$APP_NAME php artisan l5-swagger:generate

fi

docker-compose up -d

echo "Fim tudo instalado!"